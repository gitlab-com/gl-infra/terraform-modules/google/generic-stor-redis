resource "google_compute_address" "sentinel-static-ip-address" {
  count = var.sentinel_count
  name = format(
    "%v-sentinel-%02d-%v-%v-static-ip",
    var.name,
    count.index + 1 + 120,
    var.tier,
    var.environment,
  )
  address_type = "INTERNAL"
  address      = replace(var.ip_cidr_range, "/\\d+\\/\\d+$/", count.index + 1 + 120)
  subnetwork   = google_compute_subnetwork.subnetwork[0].self_link
}

resource "google_compute_disk" "sentinel_data_disk" {
  project = var.project
  count   = var.sentinel_count
  name = format(
    "%v-sentinel-%02d-%v-%v-data",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  zone = var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)
  size = var.sentinel_data_disk_size
  type = var.sentinel_data_disk_type

  labels = merge(var.labels, {
    environment  = var.environment
    pet_name     = var.name
    do_snapshots = "true"
  })

  lifecycle {
    ignore_changes = [snapshot]
  }
}

resource "google_compute_disk_resource_policy_attachment" "sentinel_data_disk_snapshot_policy_attachment" {
  count = var.sentinel_hours_between_data_disk_snapshots > 0 ? var.sentinel_count : 0

  name = google_compute_resource_policy.sentinel_data_disk_snapshot_policy[count.index].name
  disk = google_compute_disk.sentinel_data_disk[count.index].name
  zone = google_compute_disk.sentinel_data_disk[count.index].zone
  lifecycle {
    # This lifecycle rule is necessary because you cannot modify policies when
    # they are in use
    replace_triggered_by = [
      google_compute_resource_policy.sentinel_data_disk_snapshot_policy[count.index].id
    ]
  }
}

resource "google_compute_disk" "sentinel_log_disk" {
  project = var.project
  count   = var.sentinel_count
  name = format(
    "%v-sentinel-%02d-%v-%v-log",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  zone = var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)
  size = var.log_disk_size
  type = var.log_disk_type

  labels = merge(var.labels, {
    environment = var.environment
  })
}

resource "google_compute_instance" "sentinel_instance_with_attached_disk" {
  count = var.sentinel_count
  name = format(
    "%v-sentinel-%02d-%v-%v",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  machine_type = var.sentinel_machine_type

  metadata = {
    "CHEF_URL"     = var.chef_provision["server_url"]
    "CHEF_VERSION" = var.chef_provision["version"]
    "CHEF_NODE_NAME" = var.use_new_node_name ? format(
      "%v-sentinel-%02d-%v-%v.c.%v.internal",
      var.name,
      count.index + 1,
      var.tier,
      var.environment,
      var.project,
      ) : format(
      "%v-sentinel-%02d.%v.%v.%v",
      var.name,
      count.index + 1,
      var.tier,
      var.environment,
      var.dns_zone_name,
    )
    "GL_KERNEL_VERSION"       = var.kernel_version
    "CHEF_ENVIRONMENT"        = var.environment
    "CHEF_RUN_LIST"           = var.sentinel_chef_run_list
    "CHEF_DNS_ZONE_NAME"      = var.dns_zone_name
    "CHEF_PROJECT"            = var.project
    "CHEF_BOOTSTRAP_BUCKET"   = var.chef_provision["bootstrap_bucket"]
    "CHEF_BOOTSTRAP_KEYRING"  = var.chef_provision["bootstrap_keyring"]
    "CHEF_BOOTSTRAP_KEY"      = var.chef_provision["bootstrap_key"]
    "CHEF_INIT_RUN_LIST"      = var.chef_init_run_list
    "GL_PERSISTENT_DISK_PATH" = var.persistent_disk_path
    "GL_FORMAT_DATA_DISK"     = var.format_data_disk
    "block-project-ssh-keys"  = var.block_project_ssh_keys
    "enable-oslogin"          = var.enable_oslogin
    "shutdown-script"         = var.teardown_script != null ? var.teardown_script : module.bootstrap.teardown
    "startup-script"          = var.bootstrap_script != null ? var.bootstrap_script : module.bootstrap.bootstrap
  }

  project = var.project
  zone    = var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email = var.service_account_email

    // all the defaults plus cloudkms to access kms
    scopes = [
      "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/pubsub",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/cloudkms",
      "https://www.googleapis.com/auth/compute.readonly",
    ]
  }

  scheduling {
    preemptible = var.preemptible
  }

  boot_disk {
    auto_delete = true

    initialize_params {
      image  = var.os_boot_image
      size   = var.os_disk_size
      type   = var.os_disk_type
      labels = var.labels
    }
  }

  attached_disk {
    source = google_compute_disk.sentinel_data_disk[count.index].self_link
  }

  attached_disk {
    source      = google_compute_disk.sentinel_log_disk[count.index].self_link
    device_name = "log"
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnetwork[0].name
    network_ip = google_compute_address.sentinel-static-ip-address[count.index].address
    dynamic "access_config" {
      for_each = var.assign_public_ip ? [0] : []
      content {
      }
    }
  }

  labels = merge(var.labels, {
    environment = var.environment
    pet_name    = var.name
  })

  tags = [
    var.name,
    var.environment,
  ]

  lifecycle {
    ignore_changes = [min_cpu_platform, boot_disk, machine_type]
  }
}
