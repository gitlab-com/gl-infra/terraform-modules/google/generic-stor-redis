resource "google_compute_resource_policy" "redis_data_disk_snapshot_policy" {
  count = var.redis_hours_between_data_disk_snapshots > 0 ? var.redis_count : 0
  name = format(
    "%v-%02d-redis-snapshot-policy",
    var.name,
    count.index + 1,
  )

  snapshot_schedule_policy {
    schedule {
      hourly_schedule {
        hours_in_cycle = lookup(var.per_redis_node_hours_between_data_disk_snapshots, count.index + 1, var.redis_hours_between_data_disk_snapshots)
        # Time within the window to start the operations.
        # It must be in an hourly format "HH:MM", where HH : [00-23] and MM : [00] GMT. eg: 21:00
        start_time = format("%02d:00", count.index % 24)
      }
    }

    retention_policy {
      max_retention_days    = lookup(var.per_redis_node_data_disk_snapshot_max_retention_days, count.index + 1, var.redis_data_disk_snapshot_max_retention_days)
      on_source_disk_delete = "APPLY_RETENTION_POLICY"
    }

    dynamic "snapshot_properties" {
      for_each = length(var.labels) > 0 ? [true] : []
      content {
        labels = var.labels
      }
    }
  }
}

resource "google_compute_resource_policy" "sentinel_data_disk_snapshot_policy" {
  count = var.sentinel_hours_between_data_disk_snapshots > 0 ? var.sentinel_count : 0
  name = format(
    "%v-%02d-sentinel-snapshot-policy",
    var.name,
    count.index + 1,
  )

  snapshot_schedule_policy {
    schedule {
      hourly_schedule {
        hours_in_cycle = lookup(var.per_sentinel_node_hours_between_data_disk_snapshots, count.index + 1, var.sentinel_hours_between_data_disk_snapshots)
        # Time within the window to start the operations.
        # It must be in an hourly format "HH:MM", where HH : [00-23] and MM : [00] GMT. eg: 21:00
        start_time = format("%02d:00", count.index % 24)
      }
    }

    retention_policy {
      max_retention_days    = lookup(var.per_sentinel_node_data_disk_snapshot_max_retention_days, count.index + 1, var.sentinel_data_disk_snapshot_max_retention_days)
      on_source_disk_delete = "APPLY_RETENTION_POLICY"
    }

    dynamic "snapshot_properties" {
      for_each = length(var.labels) > 0 ? [true] : []
      content {
        labels = var.labels
      }
    }
  }
}
