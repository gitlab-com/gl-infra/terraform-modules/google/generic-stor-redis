variable "kernel_version" {
  type    = string
  default = ""
}

variable "use_new_node_name" {
  type    = bool
  default = false
}

variable "block_project_ssh_keys" {
  type        = string
  description = "Whether to block project level SSH keys"
  default     = "TRUE"
}

variable "chef_init_run_list" {
  type        = string
  default     = ""
  description = "run_list for the node in chef that are ran on the first boot only"
}

variable "redis_data_disk_snapshot_max_retention_days" {
  type        = number
  description = "Number of days to retain snapshots, defaults to 2 weeks"
  default     = 14
}

variable "sentinel_data_disk_snapshot_max_retention_days" {
  type        = number
  description = "Number of days to retain snapshots, defaults to 2 weeks"
  default     = 14
}

variable "persistent_disk_path" {
  type        = string
  description = "default location for disk mount"
  default     = "/var/opt/gitlab"
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"
}

variable "dns_zone_name" {
  type        = string
  description = "The GCP name of the DNS zone to use for this environment"
}

variable "egress_ports" {
  type        = list(string)
  description = "The list of ports that should be opened for egress traffic"
  default     = []
}

variable "enable_oslogin" {
  type        = string
  description = "Whether to enable OS Login GCP feature"

  # Note: setting this to TRUE breaks chef!
  # https://gitlab.com/gitlab-com/gitlab-com-infrastructure/merge_requests/297#note_66690562
  default = "FALSE"
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "redis_hours_between_data_disk_snapshots" {
  type        = number
  description = "Setting this will enable hourly data disk snapshots with staggered start times, 0 to disable"
  default     = 0
}

variable "sentinel_hours_between_data_disk_snapshots" {
  type        = number
  description = "Setting this will enable hourly data disk snapshots with staggered start times, 0 to disable"
  default     = 0
}

variable "log_disk_size" {
  type        = number
  description = "The size of the log disk"
  default     = 50
}

variable "log_disk_type" {
  type        = string
  description = "The type of the log disk"
  default     = "pd-standard"
}

variable "format_data_disk" {
  type        = string
  description = "Force formatting of the persistent disk."
  default     = "false"
}

variable "ip_cidr_range" {
  type        = string
  description = "The IP range"
}

variable "name" {
  type        = string
  description = "The pet name"
}

variable "os_boot_image" {
  type        = string
  description = "The OS image to boot"
  default     = "ubuntu-os-cloud/ubuntu-2004-lts"
}

variable "os_disk_size" {
  type        = number
  description = "The OS disk size in GiB"
  default     = 20
}

variable "os_disk_type" {
  type        = string
  description = "The OS disk type"
  default     = "pd-standard"
}

variable "per_redis_node_hours_between_data_disk_snapshots" {
  type        = map(number)
  description = "Override the number of hours between data disk snapshots (var.redis_hours_between_data_disk_snapshots) for select redis nodes. Map key should be the node number as an integer, starting at 1"
  default     = {}
}

variable "per_sentinel_node_hours_between_data_disk_snapshots" {
  type        = map(number)
  description = "Override the number of hours between data disk snapshots (var.sentinel_hours_between_data_disk_snapshots) for select sentinel nodes. Map key should be the node number as an integer, starting at 1"
  default     = {}
}

variable "per_redis_node_data_disk_snapshot_max_retention_days" {
  type        = map(number)
  description = "Override the number of days snapshots are retained for the data disk on select redis nodes. Map key should be the node number as an integer, starting at 1"
  default     = {}
}

variable "per_sentinel_node_data_disk_snapshot_max_retention_days" {
  type        = map(number)
  description = "Override the number of days snapshots are retained for the data disk on select sentinel nodes. Map key should be the node number as an integer, starting at 1"
  default     = {}
}

variable "preemptible" {
  type        = bool
  description = "Use preemptible instances for this pet"
  default     = false
}

variable "project" {
  type        = string
  description = "The project name"
}

variable "public_ports" {
  type        = list(string)
  description = "The list of ports that should be publicly reachable"
  default     = []
}

variable "redis_chef_run_list" {
  type        = string
  description = "run_list for the redis node in chef"
}

variable "redis_count" {
  type        = number
  description = "The redis nodes count"
}

variable "redis_data_disk_size" {
  type        = number
  description = "The size of the redis data disk"
  default     = 20
}

variable "redis_data_disk_type" {
  type        = string
  description = "The type of the redis data disk"
  default     = "pd-standard"
}

variable "redis_machine_type" {
  type        = string
  description = "The redis machine size"
}

variable "redis_min_cpu_platform" {
  type        = string
  description = "The redis minimum cpu platform"
  default     = ""
}

variable "region" {
  type        = string
  description = "The target region"
}

variable "sentinel_chef_run_list" {
  type        = string
  description = "run_list for the sentinel node in chef"
}

variable "sentinel_count" {
  type        = string
  description = "The redis sentinel nodes count"
}

variable "sentinel_data_disk_size" {
  type        = number
  description = "The size of the sentinel data disk"
  default     = 20
}

variable "sentinel_data_disk_type" {
  type        = string
  description = "The type of the sentinel data disk"
  default     = "pd-standard"
}

variable "sentinel_machine_type" {
  type        = string
  description = "The sentinel machine size"
}

variable "bootstrap_script" {
  type        = string
  description = "user-provided bootstrap script to override the bootstrap module"
  default     = null
}

variable "teardown_script" {
  type        = string
  description = "user-provided teardown script to override the bootstrap module"
  default     = null
}

variable "tier" {
  type        = string
  description = "The tier for this service"
}

variable "vpc" {
  type        = string
  description = "The target network"
}

variable "zone" {
  type    = string
  default = ""
}

variable "service_account_email" {
  type        = string
  description = "Service account emails under which the instance is running"
}

# Instances without public IPs cannot access the public internet without NAT.
# Ensure that a Cloud NAT instance covers the subnetwork/region for this
# instance.
variable "assign_public_ip" {
  type    = bool
  default = true
}

# Resource labels
variable "labels" {
  type        = map(string)
  description = "Labels to add to each of the managed resources."
  default     = {}
}
