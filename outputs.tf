output "instances_self_link" {
  value = google_compute_instance.instance_with_attached_disk[*].self_link
}

output "google_compute_address_static_ips" {
  value = google_compute_address.static-ip-address[*].address
}
