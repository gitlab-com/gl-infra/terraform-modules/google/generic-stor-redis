# GitLab.com Generic Storage for Redis Terraform Module

## What is this?

This module provisions Redis GCE instances.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_bootstrap"></a> [bootstrap](#module\_bootstrap) | ops.gitlab.net/gitlab-com/bootstrap/google | 5.5.8 |

## Resources

| Name | Type |
|------|------|
| [google_compute_address.sentinel-static-ip-address](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_address.static-ip-address](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_disk.data_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.redis_log_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.sentinel_data_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.sentinel_log_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk_resource_policy_attachment.redis_data_disk_snapshot_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_disk_resource_policy_attachment.sentinel_data_disk_snapshot_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_firewall.deny_all_egress](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.public](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.to_network](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.to_world](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_instance.instance_with_attached_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance) | resource |
| [google_compute_instance.sentinel_instance_with_attached_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance) | resource |
| [google_compute_resource_policy.redis_data_disk_snapshot_policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_resource_policy) | resource |
| [google_compute_resource_policy.sentinel_data_disk_snapshot_policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_resource_policy) | resource |
| [google_compute_subnetwork.subnetwork](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork) | resource |
| [google_compute_zones.available](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_zones) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_assign_public_ip"></a> [assign\_public\_ip](#input\_assign\_public\_ip) | Instances without public IPs cannot access the public internet without NAT. Ensure that a Cloud NAT instance covers the subnetwork/region for this instance. | `bool` | `true` | no |
| <a name="input_block_project_ssh_keys"></a> [block\_project\_ssh\_keys](#input\_block\_project\_ssh\_keys) | Whether to block project level SSH keys | `string` | `"TRUE"` | no |
| <a name="input_bootstrap_script"></a> [bootstrap\_script](#input\_bootstrap\_script) | user-provided bootstrap script to override the bootstrap module | `string` | `null` | no |
| <a name="input_chef_init_run_list"></a> [chef\_init\_run\_list](#input\_chef\_init\_run\_list) | run\_list for the node in chef that are ran on the first boot only | `string` | `""` | no |
| <a name="input_chef_provision"></a> [chef\_provision](#input\_chef\_provision) | Configuration details for chef server | `map(string)` | n/a | yes |
| <a name="input_dns_zone_name"></a> [dns\_zone\_name](#input\_dns\_zone\_name) | The GCP name of the DNS zone to use for this environment | `string` | n/a | yes |
| <a name="input_egress_ports"></a> [egress\_ports](#input\_egress\_ports) | The list of ports that should be opened for egress traffic | `list(string)` | `[]` | no |
| <a name="input_enable_oslogin"></a> [enable\_oslogin](#input\_enable\_oslogin) | Whether to enable OS Login GCP feature | `string` | `"FALSE"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name | `string` | n/a | yes |
| <a name="input_format_data_disk"></a> [format\_data\_disk](#input\_format\_data\_disk) | Force formatting of the persistent disk. | `string` | `"false"` | no |
| <a name="input_ip_cidr_range"></a> [ip\_cidr\_range](#input\_ip\_cidr\_range) | The IP range | `string` | n/a | yes |
| <a name="input_kernel_version"></a> [kernel\_version](#input\_kernel\_version) | n/a | `string` | `""` | no |
| <a name="input_labels"></a> [labels](#input\_labels) | Labels to add to each of the managed resources. | `map(string)` | `{}` | no |
| <a name="input_log_disk_size"></a> [log\_disk\_size](#input\_log\_disk\_size) | The size of the log disk | `number` | `50` | no |
| <a name="input_log_disk_type"></a> [log\_disk\_type](#input\_log\_disk\_type) | The type of the log disk | `string` | `"pd-standard"` | no |
| <a name="input_name"></a> [name](#input\_name) | The pet name | `string` | n/a | yes |
| <a name="input_os_boot_image"></a> [os\_boot\_image](#input\_os\_boot\_image) | The OS image to boot | `string` | `"ubuntu-os-cloud/ubuntu-2004-lts"` | no |
| <a name="input_os_disk_size"></a> [os\_disk\_size](#input\_os\_disk\_size) | The OS disk size in GiB | `number` | `20` | no |
| <a name="input_os_disk_type"></a> [os\_disk\_type](#input\_os\_disk\_type) | The OS disk type | `string` | `"pd-standard"` | no |
| <a name="input_per_redis_node_data_disk_snapshot_max_retention_days"></a> [per\_redis\_node\_data\_disk\_snapshot\_max\_retention\_days](#input\_per\_redis\_node\_data\_disk\_snapshot\_max\_retention\_days) | Override the number of days snapshots are retained for the data disk on select redis nodes. Map key should be the node number as an integer, starting at 1 | `map(number)` | `{}` | no |
| <a name="input_per_redis_node_hours_between_data_disk_snapshots"></a> [per\_redis\_node\_hours\_between\_data\_disk\_snapshots](#input\_per\_redis\_node\_hours\_between\_data\_disk\_snapshots) | Override the number of hours between data disk snapshots (var.redis\_hours\_between\_data\_disk\_snapshots) for select redis nodes. Map key should be the node number as an integer, starting at 1 | `map(number)` | `{}` | no |
| <a name="input_per_sentinel_node_data_disk_snapshot_max_retention_days"></a> [per\_sentinel\_node\_data\_disk\_snapshot\_max\_retention\_days](#input\_per\_sentinel\_node\_data\_disk\_snapshot\_max\_retention\_days) | Override the number of days snapshots are retained for the data disk on select sentinel nodes. Map key should be the node number as an integer, starting at 1 | `map(number)` | `{}` | no |
| <a name="input_per_sentinel_node_hours_between_data_disk_snapshots"></a> [per\_sentinel\_node\_hours\_between\_data\_disk\_snapshots](#input\_per\_sentinel\_node\_hours\_between\_data\_disk\_snapshots) | Override the number of hours between data disk snapshots (var.sentinel\_hours\_between\_data\_disk\_snapshots) for select sentinel nodes. Map key should be the node number as an integer, starting at 1 | `map(number)` | `{}` | no |
| <a name="input_persistent_disk_path"></a> [persistent\_disk\_path](#input\_persistent\_disk\_path) | default location for disk mount | `string` | `"/var/opt/gitlab"` | no |
| <a name="input_preemptible"></a> [preemptible](#input\_preemptible) | Use preemptible instances for this pet | `bool` | `false` | no |
| <a name="input_project"></a> [project](#input\_project) | The project name | `string` | n/a | yes |
| <a name="input_public_ports"></a> [public\_ports](#input\_public\_ports) | The list of ports that should be publicly reachable | `list(string)` | `[]` | no |
| <a name="input_redis_chef_run_list"></a> [redis\_chef\_run\_list](#input\_redis\_chef\_run\_list) | run\_list for the redis node in chef | `string` | n/a | yes |
| <a name="input_redis_count"></a> [redis\_count](#input\_redis\_count) | The redis nodes count | `number` | n/a | yes |
| <a name="input_redis_data_disk_size"></a> [redis\_data\_disk\_size](#input\_redis\_data\_disk\_size) | The size of the redis data disk | `number` | `20` | no |
| <a name="input_redis_data_disk_snapshot_max_retention_days"></a> [redis\_data\_disk\_snapshot\_max\_retention\_days](#input\_redis\_data\_disk\_snapshot\_max\_retention\_days) | Number of days to retain snapshots, defaults to 2 weeks | `number` | `14` | no |
| <a name="input_redis_data_disk_type"></a> [redis\_data\_disk\_type](#input\_redis\_data\_disk\_type) | The type of the redis data disk | `string` | `"pd-standard"` | no |
| <a name="input_redis_hours_between_data_disk_snapshots"></a> [redis\_hours\_between\_data\_disk\_snapshots](#input\_redis\_hours\_between\_data\_disk\_snapshots) | Setting this will enable hourly data disk snapshots with staggered start times, 0 to disable | `number` | `0` | no |
| <a name="input_redis_machine_type"></a> [redis\_machine\_type](#input\_redis\_machine\_type) | The redis machine size | `string` | n/a | yes |
| <a name="input_redis_min_cpu_platform"></a> [redis\_min\_cpu\_platform](#input\_redis\_min\_cpu\_platform) | The redis minimum cpu platform | `string` | `""` | no |
| <a name="input_region"></a> [region](#input\_region) | The target region | `string` | n/a | yes |
| <a name="input_sentinel_chef_run_list"></a> [sentinel\_chef\_run\_list](#input\_sentinel\_chef\_run\_list) | run\_list for the sentinel node in chef | `string` | n/a | yes |
| <a name="input_sentinel_count"></a> [sentinel\_count](#input\_sentinel\_count) | The redis sentinel nodes count | `string` | n/a | yes |
| <a name="input_sentinel_data_disk_size"></a> [sentinel\_data\_disk\_size](#input\_sentinel\_data\_disk\_size) | The size of the sentinel data disk | `number` | `20` | no |
| <a name="input_sentinel_data_disk_snapshot_max_retention_days"></a> [sentinel\_data\_disk\_snapshot\_max\_retention\_days](#input\_sentinel\_data\_disk\_snapshot\_max\_retention\_days) | Number of days to retain snapshots, defaults to 2 weeks | `number` | `14` | no |
| <a name="input_sentinel_data_disk_type"></a> [sentinel\_data\_disk\_type](#input\_sentinel\_data\_disk\_type) | The type of the sentinel data disk | `string` | `"pd-standard"` | no |
| <a name="input_sentinel_hours_between_data_disk_snapshots"></a> [sentinel\_hours\_between\_data\_disk\_snapshots](#input\_sentinel\_hours\_between\_data\_disk\_snapshots) | Setting this will enable hourly data disk snapshots with staggered start times, 0 to disable | `number` | `0` | no |
| <a name="input_sentinel_machine_type"></a> [sentinel\_machine\_type](#input\_sentinel\_machine\_type) | The sentinel machine size | `string` | n/a | yes |
| <a name="input_service_account_email"></a> [service\_account\_email](#input\_service\_account\_email) | Service account emails under which the instance is running | `string` | n/a | yes |
| <a name="input_teardown_script"></a> [teardown\_script](#input\_teardown\_script) | user-provided teardown script to override the bootstrap module | `string` | `null` | no |
| <a name="input_tier"></a> [tier](#input\_tier) | The tier for this service | `string` | n/a | yes |
| <a name="input_use_new_node_name"></a> [use\_new\_node\_name](#input\_use\_new\_node\_name) | n/a | `bool` | `false` | no |
| <a name="input_vpc"></a> [vpc](#input\_vpc) | The target network | `string` | n/a | yes |
| <a name="input_zone"></a> [zone](#input\_zone) | n/a | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_google_compute_address_static_ips"></a> [google\_compute\_address\_static\_ips](#output\_google\_compute\_address\_static\_ips) | n/a |
| <a name="output_instances_self_link"></a> [instances\_self\_link](#output\_instances\_self\_link) | n/a |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
